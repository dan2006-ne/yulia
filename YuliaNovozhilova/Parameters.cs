﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuliaNovozhilova
{
    public static class Parameters
    {
        public static double τ { get; set; } = 1;
        public static int quantityValues { get; set; } = 10;
        public static double t_max { get; set; } = 24;           
        public static int lines { get; set; } = 11;
        public static int columns { get; set; } = (int)(t_max / τ) + 1;
        public static double z_max { get; set; } = 3;
        public static double L0 { get; set; } = 3;
        public static double lyamda { get; set; } = 3*Math.Pow(10,-14);
        public static double ω { get; set; } = 2 * Math.PI / t_max;
        public static double E0 { get; set; } = 1;
        public static double E1 { get; set; } = 1;
        public static double E2 { get; set; } = 1;
        public static double A { get; set; } = 1;
        public static double D { get; set; } = 0.02;
        public static double z0 { get; set; } = 0.5;

        public static double h { get; set; } = Calculator.calculateStepH(z_max, lines);
    }
}
