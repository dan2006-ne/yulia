﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace YuliaNovozhilova
{
    public static class Calculator
    {
        public static double calculateEJijmax(double EJijMinus1, double h, double E2)
        {
            double EJijmax = 0;
            EJijmax = EJijMinus1 + h * E2;
            return EJijmax;
        }

        public static double[,] calculateEijMassiv()
        {
            double j0t = Math.Pow(10, -12);
            double[,] Eij = new double[Parameters.lines, Parameters.columns];
            Eij[0, 0] = 1;
            double[] massivZ = Determinant.determineMassivZ();
            double[] massivT = Determinant.determineMassivT();
            double[] massivE2 = Determinant.determineMassivE2(massivT);
            for (int i = 0; i < Eij.GetLength(0); i++)
            {
                Eij[i, 0] = Parameters.E0; //* Math.Exp(i * 0.5 / Parameters.L0);
            }
            for (int j = 1; j < Eij.GetLength(1); j++)
            {
                Eij[0, j] = j0t / Parameters.lyamda;
            }

            for (int j = 0; j < Eij.GetLength(1); j++)
            {
                Eij[Eij.GetLength(0) - 1, j] = Parameters.E2;
            }
            //PrintUtilities.printOutTextMassiv(Eij, ((MainWindow)System.Windows.Application.Current.MainWindow).outTable);
            for (int j = 1; j < Eij.GetLength(1); j++)
            {
                for (int i = 1; i < Eij.GetLength(0) - 1; i++)
                {
                    if (i == (Eij.GetLength(0) - 1)) //???????
                    {
                        //   Eij[i, j] = Calculator.calculateEJijmax(Eij[i, j - 1], Parameters.h, massivE2[i]);
                    }
                    else
                    {
                        //Calculator.calculate_j0t(Parameters.z_max, massivZ[j], Parameters.lyamda, massivT[j], Parameters.D, Parameters.A);
                        Eij[i, j] = Calculator.calculateEJij(Eij[i, j - 1], Eij[i - 1, j - 1], Eij[i + 1, j - 1], Parameters.lyamda, Parameters.D, j0t, Parameters.τ * j, 0.5 * i); // проверить формулу
                    }
                }
            }
            return Eij;
        }
        public static double[,] calculateEijAlternativeMassiv()
        {
            double[,] EijAlternative = new double[Parameters.lines, Parameters.columns];
            double[] massivZ = Determinant.determineMassivZ();
            double[] massivT = Determinant.determineMassivT();
            double[] massivE2 = Determinant.determineMassivE2(massivT);
            //Вторая Е формула
            for (int i = 0; i < EijAlternative.GetLength(0); i++)
            {
                EijAlternative[i, 0] = Parameters.E0;
            }
            for (int j = 1; j < EijAlternative.GetLength(1); j++)
            {
                EijAlternative[0, j] = Parameters.E1;
            }

            for (int j = 1; j < EijAlternative.GetLength(1); j++)
            {
                EijAlternative[EijAlternative.GetLength(0) - 1, j] = Parameters.E2;
            }

            for (int j = 1; j < EijAlternative.GetLength(1); j++)
            {
                for (int i = 1; i < EijAlternative.GetLength(0); i++)
                {
                    if (i == (EijAlternative.GetLength(0) - 1)) //??????
                    {
                        //EijAlternative[i, j] = Calculator.calculateEJijmax(EijAlternative[i, j - 1], Parameters.h, massivE2[i]);
                    }
                    else
                        EijAlternative[i, j] = Calculator.calculate_E_alternative(Parameters.z_max, massivZ[j], massivT[j], Parameters.A); //проверить формулу
                }
            }
            return EijAlternative;
        }

        public static double calculateStepH(double z_max, double nz)
        {
            double h = z_max / (nz - 1);
            return h;
        }
        public static double calculateStepW(double t_max, double nt)
        {
            double w = t_max / (nt - 1);
            return w;
        }

        public static double calculateEJij(double EJiPrew, double EJiPrewMinus1, double EJiPrewPlus1, double lyamda, double D, double j0t, double τ, double z)
        {
            double EJij = 0;
            //старая формула
            #region
            //double a = j0t; //4 * Math.PI * 
            //double b = D / Math.Pow(h, 2);
            //double c = EJiPrewPlus1 - 2 * EJiPrew + EJiPrewMinus1;
            //double f = 4 * Math.PI * lyamda * EJiPrew;
            // EJij = EJiPrew + τ * (a + b * c - f);
            #endregion
            EJij = (j0t / (lyamda * Math.Exp(z / Parameters.L0))) * (2 - Math.Cos(Parameters.ω * τ)); //классический электродный слой
                                                                                                      /*   double f = Math.Cos(Math.PI * Parameters.z0 / Parameters.L0);
                                                                                                         double B1 = Parameters.E1 * Math.Pow(2 / Parameters.L0, 0.5) * (((Math.Exp(Parameters.z0 / Parameters.L0) * (Math.Sin(Math.PI * Parameters.z0 / Parameters.L0) + f * Math.PI)) - Math.PI) / (1 / Parameters.L0 + Math.Pow(Math.PI, 2) / Parameters.L0) + ((Parameters.L0 / Math.PI) * (f - 1)));
                                                                                                         double P1 = 4 * Math.PI * lyamda + D * Math.Pow((Math.PI / Parameters.L0), 2);
                                                                                                         double a = Math.Exp(-P1 * τ);
                                                                                                         double b = Parameters.E1 * a * B1 * Math.Pow(2 / Parameters.L0, 0.5);
                                                                                                         double c = a / (Math.Pow(Parameters.ω, 2) + Math.Pow(P1, 2));
                                                                                                         double k = Math.Cos(Parameters.ω * τ) - Parameters.ω * Math.Sin(Parameters.ω * τ);
                                                                                                         double m = Math.Pow(2 / Parameters.L0, 0.5) * Math.Sin(Math.PI*z/ Parameters.L0);



                                                                                                         EJij = Parameters.E1+(b -c*(a*k-P1))*m;*/
            return EJij;
        }
        public static double calculate_E_alternative(double z_max, double z, double t, double A)
        {
            double e = 0;
            e = 1 + A * z * t * (z_max - z);
            return e;
        }

        public static double calculate_j0t(double z_max, double z, double lyamda, double t, double D, double A)
        {
            double j0t = 0;
            j0t = A * z * (z_max - z) + 4 * Math.PI * lyamda * (1 + A * (t + Parameters.τ / 2) * z * (z_max - z)) + D * 2 * A * (t + Parameters.τ / 2);
            // j0t = j0t / (4 * Math.PI);
            return j0t;
        }
        public static double[,] calculateMetodProgonky()
        {
            double[] A = calculateCoefficientA();
            double[] B = calculateCoefficientB();
            double[] C = calculateCoefficientC();
            double[] F = new double[Parameters.lines];
            double[] α = new double[Parameters.lines];
            double[] β = new double[Parameters.lines];
            double[,] Eij = new double[Parameters.lines, Parameters.columns];
            for (int i = 0; i < Eij.GetLength(0); i++)
            {
                Eij[i, 0] = Parameters.E0;
            }
            for (int j = 1; j < Eij.GetLength(1); j++)
            {
                Eij[0, j] = Parameters.E1;
            }
            for (int j = 1; j < Eij.GetLength(1); j++)
            {
                Eij[Eij.GetLength(0) - 1, j] = Parameters.E2;
            }
            for (int j = 1; j < Eij.GetLongLength(1); j++)
            {
                F = calculateCoefficientF(Eij, j - 1);
                α[0] = 0;
                β[0] = 0;
                for (int i = 1; i < Eij.GetLongLength(0); i++)
                {
                    α[i] = -C[i - 1] / (A[i - 1] * α[i - 1] + B[i - 1]);
                    β[i] = (F[i - 1] - A[i - 1] * β[i - 1]) / (A[i - 1] * α[i - 1] + B[i - 1]);
                }
                for (int i = Parameters.lines - 1; i >= 0; i--)
                {
                    if (i == Parameters.lines - 1)
                    {
                        Eij[i, j] = (F[i] - A[i] * β[i]) / (A[i] * α[i] + B[i]);
                    }
                    else
                    {
                        Eij[i, j] = α[i + 1] * Eij[i + 1, j] + β[i + 1];
                    }
                }

            }
            return Eij;
        }
        public static double[] calculateCoefficientA()
        {
            double[] A = new double[Parameters.lines];
            for (int i = 0; i < A.GetLength(0); i++)
            {
                if (i == 0)
                {
                    A[i] = 0;
                }
                else if (i == A.GetLength(0) - 1)
                {
                    A[i] = 0;
                }
                else
                    A[i] = (-Parameters.D * Parameters.τ) / (2 * Math.Pow(Parameters.h, 2));
            }
            return A;
        }
        public static double[] calculateCoefficientB()
        {
            double[] B = new double[Parameters.lines];
            for (int i = 0; i < B.GetLength(0); i++)
            {
                if (i == 0)
                {
                    B[i] = 1;
                }
                else if (i == B.GetLength(0) - 1)
                {
                    B[i] = 1;
                }
                else
                    B[i] = 1 + 2 * Math.PI * Parameters.lyamda * Parameters.τ + (Parameters.D * Parameters.τ) / (Math.Pow(Parameters.h, 2));
            }
            return B;
        }
        public static double[] calculateCoefficientC()
        {
            double[] C = new double[Parameters.lines];
            for (int i = 0; i < C.GetLength(0); i++)
            {
                if (i == 0)
                {
                    C[i] = 0;
                }
                else if (i == C.GetLength(0) - 1)
                {
                    C[i] = 0;
                }
                else
                    C[i] = (-Parameters.D * Parameters.τ) / (2 * Math.Pow(Parameters.h, 2));
            }
            return C;
        }
        public static double[] calculateCoefficientF(double[,] Eij, int j)
        {
            double[] massivZ = Determinant.determineMassivZ();
            double[] massivT = Determinant.determineMassivT();
            double j0t = 1;
            double[] F = new double[Parameters.lines];
            F[0] = 1;
            F[F.GetLength(0) - 1] = 1;
            for (int i = 1; i < Eij.GetLongLength(0) - 1; i++)
            {
                F[i] = Eij[i, j] - 2 * Math.PI * Parameters.lyamda * Parameters.τ * Eij[i, j] + (Parameters.D * Parameters.τ) / (2 * Math.Pow(Parameters.h, 2)) * (Eij[i + 1, j] - 2 * Eij[i, j] + Eij[i - 1, j]) + 2 * Math.PI * Parameters.τ * j0t;
            }

            return F;
        }
    }
}
