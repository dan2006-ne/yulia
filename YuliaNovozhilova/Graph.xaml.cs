﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;

namespace YuliaNovozhilova
{
    /// <summary>
    /// Логика взаимодействия для Graph.xaml
    /// </summary>
    public partial class Graph : Window
    {
        public Graph()
        {

            InitializeComponent();
            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Series 1",
                    Values = new ChartValues<double> { 0 }
                },
                //new LineSeries
                //{
                //    Title = "Series 2",
                //    Values = new ChartValues<double> { 6, 7, 3, 4 ,6 },
                //    PointGeometry = null
                //},
                //new LineSeries
                //{
                //    Title = "Series 3",
                //    Values = new ChartValues<double> { 4,2,7,2,7 },
                //    PointGeometry = DefaultGeometries.Square,
                //    PointGeometrySize = 15
                //}
            };

            //  Labels = new[] { "Jan", "Feb", "Mar", "Apr", "May" };
            //  YFormatter = value => value.ToString("C");

            //modifying the series collection will animate and update the chart
            //SeriesCollection.Add(new LineSeries
            //{
            //    Title = "Series 4",
            //    Values = new ChartValues<double> { 5, 3, 2, 4 },
            //    LineSmoothness = 0, //0: straight lines, 1: really smooth lines
            //    PointGeometry = Geometry.Parse("m 25 70.36218 20 -28 -20 22 -8 -6 z"),
            //    PointGeometrySize = 50,
            //    PointForeground = Brushes.Gray
            //});

            //modifying any series values will also animate and update the chart
            // SeriesCollection[3].Values.Add(5d);

            DataContext = this;
        }
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }



        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (slValueτ.Value != 0)
            {
                Parameters.τ = slValueτ.Value;
                Parameters.columns = (int)(Parameters.t_max / Parameters.τ) + 1;
                double[,] Eij = Calculator.calculateEijMassiv(); 
                double [,] EijResult= new double[Eij.GetLength(0), Eij.GetLength(1) - 1];
                for (int i = 0; i < EijResult.GetLength(0); i++)
                {
                    for (int j = 0; j < EijResult.GetLength(1); j++)
                    {
                        EijResult[i, j] = Eij[i, j + 1];
                    }
                }
                SeriesCollection[0].Values = stringOfMassiv(EijResult, (int)slValuei.Value);
            }
        }

        private ChartValues<double> stringOfMassiv(double[,] massiv, int i)
        {
            var list = new ChartValues<double>();
            for (int j = 0; j < massiv.GetLength(1); j++)
            {
                list.Add(massiv[i, j]);
            }
            return list;
        }
    }
}
