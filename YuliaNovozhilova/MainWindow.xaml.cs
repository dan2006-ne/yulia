﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;

namespace YuliaNovozhilova
{
    /// <summary>
    /// основная форма
    /// </summary>
    public partial class MainWindow : Window
    {
        public SeriesCollection SeriesCollection { get; private set; }

        public MainWindow()
        {
            InitializeComponent();


        }

        private void Button_Eij(object sender, RoutedEventArgs e)
        {
            double[,] Eij = Calculator.calculateEijMassiv();
            double[,] EijAlternative = Calculator.calculateEijAlternativeMassiv();
            double[] massivT = Determinant.determineMassivT();
            //  PrintUtilities.printMassivAndTtoFile(Eij, EijAlternative, massivT, "итог.txt", 10, 1);// вывод в файл
            outTable.Items.Add(new { i = "Eij:" });
            PrintUtilities.printOutTextMassiv(Eij, outTable);
            //outTable.Items.Add(new { i = "EijAlt:" });
           // PrintUtilities.printOutTextMassiv(EijAlternative, outTable);
        }

        private void Button_Metod_Progonky(object sender, RoutedEventArgs e)
        {
            
            double[,] massivX = Calculator.calculateMetodProgonky();
            outTable.Items.Add(new { i = "XEij:" });
            PrintUtilities.printOutTextMassiv(massivX, outTable);
        }

        private void Button_Parametres_change(object sender, RoutedEventArgs e)
        {
            Window1 w1 = new Window1();
            w1.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            outTable.Columns.Clear();
            outTable.Items.Clear();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Window Graph = new Graph();
            Graph.Show();

        }
        private void Button_test(object sender, RoutedEventArgs e)
        {
            double[] A = { 0,-1,1,1};
            double[] B = { 4, 3, 4, 7 };
            double[] C = { -1, 1, 1, 0 };
            double[] F = { 7, 0, 2, 34 };
            double[] α = new double[4];
            double[] β = new double[4];
            double[,] Eij = new double[4, 1];
            for (int j = 0; j < Eij.GetLongLength(1); j++)
            {                
                α[0] = 0;
                β[0] = 0;
                for (int i = 1; i < Eij.GetLongLength(0); i++)
                {
                    α[i] = -C[i - 1] / (A[i - 1] * α[i - 1] + B[i - 1]);
                    β[i] = (F[i - 1] - A[i - 1] * β[i - 1]) / (A[i - 1] * α[i - 1] + B[i - 1]);
                }
                for (int i = 4 - 1; i >= 0; i--)
                {
                    if (i == 4 - 1)
                    {
                        Eij[i, j] = (F[i] - A[i] * β[i]) / (A[i] * α[i] + B[i]);
                    }
                    else
                    {
                        Eij[i, j] = α[i + 1] * Eij[i + 1, j] + β[i + 1];
                    }
                }
            }
            PrintUtilities.printOutTextMassiv(Eij, outTable);
        }
    }
}

