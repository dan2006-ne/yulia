﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuliaNovozhilova
{
    class Determinant
    {
        public static double[] determineMassivT()
        {
            double[] massivT = new double[Parameters.columns];
            for (int i = 1; i < massivT.Length; i++)
            {
                massivT[i] = massivT[i - 1] + Parameters.τ;               
            }
            return massivT;
        }
        public static double[] determineMassivZ()
        {
            double[] massivZ = new double[Parameters.columns];
            for (int i = 1; i < massivZ.Length; i++)
            {
                massivZ[i] = massivZ[i - 1] + Parameters.z_max / (massivZ.Length - 1);
                massivZ[i] = Math.Round(massivZ[i], 2);
            }
            return massivZ;
        }
        public static double[] determineMassivE2(double[] massivT)
        {
            double[] massivE2 = new double[Parameters.columns];
            for (int i = 1; i < massivE2.Length; i++)
            {
                 massivE2[i] = -Parameters.A * massivT[i] * Parameters.z_max;
             //   massivE2[i] = 1;
            }
            return massivE2;
        }
    }
}
