﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace YuliaNovozhilova
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (textBox_τ.Text != "")
            {
                Parameters.τ = Convert.ToDouble(textBox_τ.Text);
                Parameters.columns = (int)(Parameters.t_max / Parameters.τ) + 1;
            }
            if (textBoxQuantityValues.Text != "")
            {
                Parameters.quantityValues = Convert.ToInt32(textBoxQuantityValues.Text);                
            }
            if (textBoxL0.Text != "")
            {
                Parameters.L0 = Convert.ToDouble(textBoxL0.Text);
            }
            this.Close();
        }
    }
}
