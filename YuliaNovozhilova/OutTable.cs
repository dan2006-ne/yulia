﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuliaNovozhilova
{
    public class OutTable
    {
        public OutTable(int I, string Vocalist, string Album, int Year)
        {
            this.I = I;
            this.Vocalist = Vocalist;
            this.Album = Album;
            this.Year = Year;
        }
        public int I { get; set; }
        public string Vocalist { get; set; }
        public string Album { get; set; }
        public int Year { get; set; }
    }
}
