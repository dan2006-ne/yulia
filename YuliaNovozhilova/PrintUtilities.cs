﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using Newtonsoft.Json;
namespace YuliaNovozhilova
{
    public static class PrintUtilities
    {
        public static void printOutTextMassiv(double[,] massiv, DataGrid outTable)
        {
            var dict = new Dictionary<string, string>();
            string[,] outMassiv = new string[Parameters.lines+1, 26];
            double step = massiv.GetLength(1) / 25.0;
            step = Math.Ceiling(step);
            int stepint = (int)step;
            
            string headerOf0Column = "";
            int nameOfCollums = 0;
            if (outTable.Columns.Count > 0)
                headerOf0Column = outTable.Columns[0].Header.ToString();
            if (headerOf0Column != " ")
            {
                DataGridTextColumn textColumn = new DataGridTextColumn();
                textColumn.Header = " ";
                textColumn.Binding = new Binding("i");
                outTable.Columns.Add(textColumn);
                dict.Add("i", "i");
            }
            
            for (int i = 0; i < 26; i++)
            {
                if (outTable.Columns.Count <= i + 1)
                {
                    DataGridTextColumn textColumn = new DataGridTextColumn();
                    textColumn.Header = "j: " + nameOfCollums.ToString();
                    textColumn.Binding = new Binding($"_{ i.ToString()}");
                    outTable.Columns.Add(textColumn);
                    nameOfCollums += stepint;
                }
            }
            for (int i = 0; i < massiv.GetLength(0); i ++ )                
            {
                int k = 0;
                for (int j = 0; j < massiv.GetLength(1); j+= stepint)
                {
                    outMassiv[i, k] = massiv[i, j].ToString("G4", CultureInfo.InvariantCulture);                    
                    k++;
                    if (k > 25)
                        break;
                }
            }
            for (int i = 0; i < massiv.GetLength(0); i++)
            {
                string si = "i: " + i.ToString();
                outTable.Items.Add(new
                {
                    i = si,
                    _0 = outMassiv[i, 0],
                    _1 = outMassiv[i, 1],
                    _2 = outMassiv[i, 2],
                    _3 = outMassiv[i, 3],
                    _4 = outMassiv[i, 4],
                    _5 = outMassiv[i, 5],
                    _6 = outMassiv[i, 6],
                    _7 = outMassiv[i, 7],
                    _8 = outMassiv[i, 8],
                    _9 = outMassiv[i, 9],
                    _10 = outMassiv[i, 10],
                    _11 = outMassiv[i, 11],
                    _12 = outMassiv[i, 12],
                    _13 = outMassiv[i, 13],
                    _14 = outMassiv[i, 14],
                    _15 = outMassiv[i, 15],
                    _16 = outMassiv[i, 16],
                    _17 = outMassiv[i, 17],
                    _18 = outMassiv[i, 18],
                    _19 = outMassiv[i, 19],
                    _20 = outMassiv[i, 20],
                    _21 = outMassiv[i, 21],
                    _22 = outMassiv[i, 22],
                    _23 = outMassiv[i, 23],
                    _24 = outMassiv[i, 24],
                    _25 = outMassiv[i, 25],
                });
            }
            outTable.Items.Add(new { });
        }

        public static void printOutText(string outText, TextBox outTextBox)
        {
            outTextBox.Text += "\n";
            outTextBox.Text += outText;
        }

        public static void printMassivAndTtoFile(double[,] massivE, double[,] massivEAlternative, double[] massivT, string fileName, int stepi, int stepj)
        {
            File.WriteAllText(fileName, "");
            string Ej1s = "";
            string EAlternative = "";
            string strDifference = "";
            double numDifference = 0;
            int longDifference = 0;
            int longEj1 = 0;
            int longEAlternative = 0;
            int maxLong = 0;
            for (int i = 0; i < massivE.GetLength(0); i++)
            {
                File.AppendAllText(fileName, $"t={massivT[i]}\n");
                for (int j = 0; j < massivE.GetLength(1); j++)
                {
                    numDifference = Math.Round(massivE[i, j] - massivEAlternative[i, j], 2);
                    massivE[i, j] = Math.Round(massivE[i, j], 2);
                    massivEAlternative[i, j] = Math.Round(massivEAlternative[i, j], 2);
                    Ej1s += massivE[i, j].ToString("G4", CultureInfo.InvariantCulture);
                    EAlternative += massivEAlternative[i, j].ToString("G4", CultureInfo.InvariantCulture);
                    strDifference += numDifference.ToString("G4", CultureInfo.InvariantCulture);
                    longEj1 = Ej1s.Length;
                    longEAlternative = EAlternative.Length;
                    longDifference = strDifference.Length;
                    if (longEj1 >= longEAlternative && longEj1 >= longDifference)
                        maxLong = longEj1;
                    else if (longEAlternative >= longEj1 && longEAlternative >= longDifference)
                        maxLong = longEAlternative;
                    else if (longDifference >= longEj1 && longDifference >= longEAlternative)
                        maxLong = longDifference;
                    Ej1s = Ej1s.PadRight(maxLong);
                    EAlternative = EAlternative.PadRight(maxLong);
                    strDifference = strDifference.PadRight(maxLong);
                    Ej1s += " | ";
                    EAlternative += " | ";
                    strDifference += " | ";
                }
                File.AppendAllText(fileName, $"{Ej1s}\n");
                File.AppendAllText(fileName, $"{EAlternative}\n");
                File.AppendAllText(fileName, $"{strDifference}\n");
                File.AppendAllText(fileName, "\n");
                Ej1s = "";
                EAlternative = "";
                strDifference = "";
            }
        }

        public static void printMassivAndKoefToFile(double[,] XEij, double[,] A, double[,] B, double[,] C, double[,] F, double[,] α, double[,] β, string fileName, int stepi, int stepj)
        {
            File.WriteAllText(fileName, "");
            string istr = "i: ";
            string XEijs = "XEij: ";

            string As = "A: ";
            string Bs = "B: ";
            string Cs = "C: ";
            string Fs = "F: ";
            string αs = "α: ";
            string βs = "β: ";
            int maxLong = 0;
            for (int i = 0; i < XEij.GetLength(0); i++)
            {
                istr += i.ToString();
                for (int j = 0; j < XEij.GetLength(1); j++)
                {
                    XEij[i, j] = Math.Round(XEij[i, j], 2);
                    XEijs += XEij[i, j].ToString("G4", CultureInfo.InvariantCulture);
                    As += A[i, j].ToString("G4", CultureInfo.InvariantCulture);
                    Bs += B[i, j].ToString("G4", CultureInfo.InvariantCulture);
                    Cs += C[i, j].ToString("G4", CultureInfo.InvariantCulture);
                    Fs += F[i, j].ToString("G4", CultureInfo.InvariantCulture);
                    αs += α[i, j].ToString("G4", CultureInfo.InvariantCulture);
                    βs += β[i, j].ToString("G4", CultureInfo.InvariantCulture);

                    if (XEijs.Length >= As.Length && XEijs.Length >= Bs.Length && XEijs.Length >= Fs.Length)
                        maxLong = XEijs.Length;
                    else if (As.Length >= XEijs.Length && As.Length >= Bs.Length && As.Length >= Fs.Length)
                        maxLong = As.Length;
                    else if (Bs.Length >= XEijs.Length && Bs.Length >= As.Length && Bs.Length >= Fs.Length)
                        maxLong = Bs.Length;
                    else if (Fs.Length >= XEijs.Length && Fs.Length >= As.Length && Fs.Length >= Bs.Length)
                        maxLong = Fs.Length;
                    XEijs = XEijs.PadRight(maxLong);

                    As = As.PadRight(maxLong);
                    Bs = Bs.PadRight(maxLong);
                    Cs = Cs.PadRight(maxLong);
                    Fs = Fs.PadRight(maxLong);
                    istr = istr.PadRight(maxLong);
                    αs = αs.PadRight(maxLong);
                    βs = βs.PadRight(maxLong);


                    XEijs += " | ";
                    As += " | ";
                    Bs += " | ";
                    Cs += " | ";
                    Fs += " | ";
                    istr += " | ";
                    αs += " | ";
                    βs += " | ";
                }

                File.AppendAllText(fileName, $"{XEijs}\n");
                File.AppendAllText(fileName, $"{As}\n");
                File.AppendAllText(fileName, $"{Bs}\n");
                File.AppendAllText(fileName, $"{Cs}\n");
                File.AppendAllText(fileName, $"{Fs}\n");
                File.AppendAllText(fileName, $"{istr}\n");
                File.AppendAllText(fileName, $"{αs}\n");
                File.AppendAllText(fileName, $"{βs}\n");
                File.AppendAllText(fileName, "\n");

                XEijs = "XEij: ";
                As = "A: ";
                Bs = "B: ";
                Cs = "C: ";
                Fs = "F: ";
                istr = "i: ";
                αs = "α: ";
                βs = "β: ";
            }
        }
    }
}
