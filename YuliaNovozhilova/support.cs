﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuliaNovozhilova
{
    public static class support
    {
        public static string convertDoubleMassivToString(double[] massiv, int accuracy)
        {
            string rezult = "";
            for (int i = 0; i < massiv.Length; i++)
            {
                rezult += massiv[i].ToString($"F{accuracy}")+"  |  ";
            }
            return rezult;
        }
    }
}
